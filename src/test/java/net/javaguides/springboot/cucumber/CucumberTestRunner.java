package net.javaguides.springboot.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature", plugin = {"pretty", "html:target/cucumber"})
public class CucumberTestRunner {

}
