package net.javaguides.springboot.cucumber;


import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UserControllerStepDefinitions extends SpringIntegrationAbstractTest{
    String baseUrl = DEFAULT_URL;
    HttpHeaders headers;
    ResponseEntity<String> response;
    
    @Given("^I set .* user service api endpoint$")
    public void setEndpoint(){
    	baseUrl = DEFAULT_URL + "/api/users";
        System.out.println("Add URL :"+baseUrl);
    }
    
    @When ("^I set request HEADER$")
    public void setHeders(){
    	headers = new HttpHeaders();
    	headers.add("Accept", "application/json");
    	headers.add("Content-Type", "application/json");
    }    
    
    @When ("^Send a POST HTTP request$")
    public void sendPostRequest(){

        String jsonBody = "{\"id\":1,\"firstName\":\"JAMES\",\"lastName\":\"YOUNG\",\"email\":\"JAMES@GMAIL.COM\"}";
        HttpEntity<String> entity = new HttpEntity<String>(jsonBody, headers);       
        response = restTemplate.postForEntity(baseUrl, entity, String.class);
    }
    
//    @Then ("I receive a valid response")
//    public void verifyPostResponse(){
// 
//        String responseBody = response.getBody().toString();
//
//        Assert.hasText(responseBody,"firstName");
//         
//        // Check if the status code is 201
//        Assert.isTrue(response.getStatusCode() == HttpStatus.CREATED, "statsu 201");
//    }  
    
    @Then ("I receive a valid response code {int}")
    public void verifyResponseCode(int statusCode){
 
        Assert.isTrue(response.getStatusCode() == HttpStatus.valueOf(statusCode), "StatusCode::"+statusCode);
    }     
    
    @When ("Send a GET HTTP request")
    public void sendGetRequest(){
        response = restTemplate.getForEntity(baseUrl, String.class);
    }     
    
    @When ("Send a GET HTTP request with id {int}")
    public void sendGetRequestWithId(int id){
        Map < String, String > params = new HashMap < String, String > ();
        params.put("id", "1");    	
        response = restTemplate.getForEntity(baseUrl, String.class, params);
    }     

    @When ("Send a PUT HTTP request Body with id {int}")
    public void sendPutRequest(int id){
        Map < String, String > params = new HashMap < String, String > ();
        params.put("id", "1");
        
        String requestBody = "{\"id\":1,\"firstName\":\"RAM\",\"lastName\":\"YOUNG\",\"email\":\"RAM@GMAIL.COM\"}";
        
        HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers); 
        response = restTemplate.exchange(baseUrl+"/"+id, HttpMethod.PUT, entity, String.class);   

    }
    
    @When ("Send a DELETE HTTP request with id {int}")
    public void sendDeleteRequest(int id){
        Map < String, String > params = new HashMap < String, String > ();
        params.put("id", "1");
        
        //HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers); 
        response = restTemplate.exchange(baseUrl+"/"+id, HttpMethod.DELETE, null, String.class); 
    }       
    
}
