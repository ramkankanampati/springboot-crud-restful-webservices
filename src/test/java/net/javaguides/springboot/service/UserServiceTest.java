package net.javaguides.springboot.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import net.javaguides.springboot.entity.User;
import net.javaguides.springboot.repository.UserRepository;

//@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    
    @InjectMocks
    private UserService userService;
    
//    @Before
//    public void setUp(){
//        MockitoAnnotations.initMocks(this);
//    }
    
    @Test
    void shouldSavedUserSuccessFully() {
        final User user = new User(null, "ten@mail.com","teten","teten");

        //given(userRepository.findByEmail(user.getEmail())).willReturn(Optional.empty());
        given(userRepository.save(any(User.class))).willReturn(user);

        User savedUser = userService.createUser(user);

        assertThat(savedUser).isNotNull();

        verify(userRepository).save(any(User.class));

    }


    @Test
    void updateUser() {
        final User user = new User(1L, "ten@mail.com","teten","teten");

        given(userRepository.save(user)).willReturn(user);

        final User expected = userService.updateUser(user);

        assertThat(expected).isNotNull();

        verify(userRepository).save(any(User.class));
    }

    @Test
    void shouldReturnFindAll() {
        List<User> datas = new ArrayList<User>();
        datas.add(new User(1L, "ten@mail.com","teten","teten"));
        datas.add(new User(2L, "ten@mail.com","teten","teten"));
        datas.add(new User(3L, "ten@mail.com","teten","teten"));

        given(userRepository.findAll()).willReturn(datas);

        List<User> expected = userService.findAllUsers();

        assertEquals(expected, datas);
    }

    @Test
    void findUserById(){
        final Long id = 1L;
        final User user = new User(1L, "ten@mail.com","teten","teten");

        given(userRepository.findById(id)).willReturn(Optional.of(user));

        final Optional<User> expected  =userService.findUserById(id);

        assertThat(expected).isNotNull();

    }

    @Test
    void shouldBeDelete() {
        final Long userId=1L;

        userService.deleteUserById(userId);
        userService.deleteUserById(userId);

        verify(userRepository, times(2)).deleteById(userId);
    }

}