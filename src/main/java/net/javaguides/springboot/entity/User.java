package net.javaguides.springboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
    @NotEmpty(message = "email should not be empty")
    @Column(name = "email", nullable = false, length = 200)
	private String email;
	
	@NotEmpty(message = "firstName should not be empty")
    @Column(name = "first_name", nullable = false, length = 100)
	private String firstName;	

	@NotEmpty(message = "lastName should not be empty")
    @Column(name = "last_name", nullable = false, length = 100)
	private String lastName;

}
