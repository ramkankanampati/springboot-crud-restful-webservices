package net.javaguides.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.javaguides.springboot.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	//This includes all the methods from JPARepository and below additional methods
	
//    @Query("select u from User u where u.email=?1 and u.password=?2")
//    Optional<User> login(String email, String password);
//
//    Optional<User> findByEmail(String email);
}
