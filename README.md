-- Useful tutorial links for Mockito testing --
https://github.com/teten777/spring-boot-rest-api/blob/master/src/main/java/id/test/springboottesting/model/User.java

-- Rest API testing with cucumber --
https://www.baeldung.com/cucumber-rest-api-testing

https://examples.javacodegeeks.com/enterprise-java/spring/boot/spring-boot-cucumber-tests-example/

https://github.com/eugenp/tutorials/blob/master/testing-modules/rest-testing/src/test/java/com/baeldung/rest/cucumber/StepDefinition.java

--- Best cucumber read ---
https://thepracticaldeveloper.com/cucumber-guide-1-intro-bdd-gherkin/




--- CREATE DATABASE --

create database usersDB;

use usersDB;


CREATE TABLE IF NOT EXISTS usersDB.users (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

 -- ACCESS SWAGGER ---
 1) Start application in eclipse
 
 2) Get swagger definition using below link and save that  data into a file 'swagger-definition-output.json'
      http://localhost:8080/v2/api-docs
      
 3) Import that file to SOAP UI as a Swagger project
 
 4) You can test APIs in SWAGGER also by using below link    
 http://localhost:8080/swagger-ui/index.html
 
 -- To add lombok to eclipse on MAC---
Step 1  Edit /Users/ram/eclipse/jee-2020-06/Eclipse.app/Contents/MacOS/eclipse/eclipse.ini with
just after 
-vmargs

add below
-Xbootclasspath/a:lombok.jar
-javaagent:lombok.jar



Step 2 paste lombok.jar to /Users/ram/eclipse/jee-2020-06/Eclipse.app/Contents/MacOS

MacBook-Pro:MacOS ram$ pwd
/Users/ram/eclipse/jee-2020-06/Eclipse.app/Contents/MacOS

MacBook-Pro:MacOS ram$ cp /Users/ram/.m2/repository/org/projectlombok/lombok/1.18.12/lombok-1.18.12.jar .

MacBook-Pro:MacOS ram$ mv lombok-1.18.12.jar lombak.jar

Step 3 After Eclipse restart: project > clean

---- CUCUMBER RUN ---
Run CucumberTestRunner.java class
 
 
        